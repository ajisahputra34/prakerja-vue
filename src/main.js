import { createApp } from 'vue'
import App from './App.vue'
import { createRouter, createWebHistory } from 'vue-router'
import Home from './pages/Home.vue'
import Welcome from "./pages/Welcome.vue"
import './style.css'

const routes = [
    { path: '/', component: Welcome },
    { path: '/home', component: Home },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

const app = createApp(App)
app.use(router)

app.mount('#app')
