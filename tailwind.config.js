/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
        colors: {
          'myred': '#e60316',
          'myorange': '#ff5100',
          'ungu': '#891f79',
          'unguku': 'rgb(97, 6, 97)',
          'primary': '#007bff',
        },
        height: {
          '100' : '540px',
        },
        fontFamily: {
          'raleway' : ['Raleway', 'sans-serif'],
          'viga' : ['Viga', 'sans-serif'],
        },
        fontSize: {
          '52': '52px',
        }
    },
  },
  plugins: [
    function ({ addUtilities }) {
      addUtilities({
        '.text-shadow-custom': {
          'text-shadow': '1px 1px 1px rgba(0, 0, 0, 0.9)',
        },
      });
    },
  ],
}